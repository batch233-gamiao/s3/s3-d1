-- SQL CRUD OPERATIONS

/*
	1. (Create) Adding a Record
		Terminal
		Adding a Record:
		Syntax
			INSERT INTO table_name (column_name) VALUES (values1);
*/
	INSERT INTO artists (name) VALUES ("Nirvana");

	-- Mini Activity
	INSERT INTO artists (name) VALUES ("Eminem");
	INSERT INTO artists (name) VALUES ("John Legend");

/*
	2. (Read) Show all records
		Terminal
			Displaying / retrieving records
			Syntax
				SELECT column_name FROM table_name;				
*/
	SELECT name FROM artists;

/*
	3. (Create) Adding record with multiple columns
		Terminal
			Adding a record with multiple columns
			Syntax
				INSERT INTO table_name (column_name, column_name) VALUES (value1,value2);
*/
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Nevermind", "1991-09-24", 1);

	-- Mini Activity 
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Music to Be Murdered By", "2020-01-17", 2);
	INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Love in the Future", "2013-08-30", 3);

/*
	4. (Create) Adding multiple records
		Terminal
			Adding multiple records
			Syntax
			INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);
*/
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Smell Like A Teen Spirit", 501, "Grunge", 1);

	-- Mini Activity 
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Godzilla", 331, "Hip-Hop", 2);
	INSERT INTO songs (song_name, length, genre, album_id) VALUES ("All of me", 430, "R&B", 3);

/*
	5. Show records with selected columns
		Terminal
			Retrieving records with selected columns
			Syntax
			SELECT (column_name1, column_name2) FROM table_name;
*/
	SELECT song_name, genre FROM songs;

	-- Mini Activity
	SELECT album_title, date_released FROM albums;

/*
	6. Show records that meet a certain condition
		Terminal
			Retrieving records with certain conditions
			Syntax
				SELECT column_name1 FROM table_name WHERE condition;			
*/
	SELECT song_name FROM songs WHERE genre = "Hip-Hop";

/*
	7. Show records with multiple condition
		Terminal
			Displaying / retrieving records with multiple conditions
			Syntax
				AND Clause
					SELECT column_name FROM table_name WHERE condition1 AND condition2;	
				OR Clause
					SELECT column_name FROM table_name WHERE condition1 OR condition2;	
				-- we can use AND and OR keyword for multiple expressions in the WHERE clause		
*/
	SELECT song_name, length FROM songs WHERE length > 314 AND genre = "Grunge";
	-- at least one of the condition was met
	SELECT song_name, length FROM songs WHERE length > 314 OR genre = "Grunge";
	-- all conditions must be met
	SELECT * FROM songs WHERE length > 350 OR genre = "GRUNGE";

/*
	8. (Update) Updating Records
		Terminal
			Add a record to update:
				INSERT INTO artists (name) VALUES ("Incubus");	
				INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Monuments and Melodies", "2009-06-16", 4);
				INSERT INTO songs(song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 4);
			Updating records;
			Syntax
				UPDATE table_name SET column name = value WHERE condition;
				UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;	
*/
	UPDATE songs SET genre = "Rock" WHERE length > 400;
	UPDATE songs SET length = 240 WHERE song_name = "Godzilla";

/*
	9. (Delete) Deleting records
		Terminal
			Deleting records
			Syntax
				DELETE FROM table_name WHERE condition;
*/

-- Mini Activity
	-- Insert 2 users
	INSERT INTO users (username,password) VALUES ("jgamiao0221","password123");
	INSERT INTO users (username,password) VALUES ("sample2","samplepassword");

	-- Insert 2 tasks
	INSERT INTO tasks (status,name,user_id) VALUES ("active","sleeping",1);
	INSERT INTO tasks (status,name,user_id) VALUES ("inactive","eating",2);

	-- Retrieve users and tasks table
	SELECT * FROM users;
	SELECT * FROM tasks;

